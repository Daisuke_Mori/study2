<?php
Yii::app()->clientScript->registerCssfile(Yii::app()->baseUrl.'/css/piano.css');
Yii::log(__CLASS__.":".__FUNCTION__.":".__LINE__,CLogger::LEVEL_ERROR);

/**
 * @var Iberia[] $iberias
 */
$iberias = Iberia::model()->findAll();
?>

<?php foreach ($iberias as $iberia):?>
    <div class="iberia">
        <div class="number"><?php echo "第",$iberia->number,"曲"; ?></div>
        <div class="title"><?php echo $iberia->title; ?></div>
        <div class="time"><?php echo "演奏時間は",$iberia->time,"分"; ?></div>
        <div class="music_key"><?php echo $iberia->music_key; ?></div>
    </div>
<?php endforeach; ?>

<form action="/site/piano" method="post">
    <div>
        <div class="label"><label>曲順 : </label></div><input name="number">
    </div>
    <div>
        <div class="label"><label>曲名 : </label></div><input name="title">
    </div>
    <div>
        <div class="label"><label>演奏時間 : </label></div><input name="time">
    </div>
    <div>
        <div class="label"><label>調 : </label></div><input name="music_key">
    </div>
    <input type="submit" value="登録">
</form>
