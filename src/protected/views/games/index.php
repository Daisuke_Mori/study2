<?php
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/games.css?v=' . time());
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/index.min.js?v=' . time(), CClientScript::POS_END);
?>

<div class="players">
    player1:○ player2:×
</div>

<div >
    <span class="teban">player1</span>
    <span>の手番です。</span>
</div>


<div class="boxes">
    <?php for ($y = 1; $y <= 3; $y++): ?>
        <div class="box_<?php echo $y ?>">
            <?php for ($x = 1; $x <= 3; $x++): ?>
                <div class="box" data-x="<?php echo $x; ?>" data-y="<?php echo $y; ?>">
                    <div class="maru_batsu">&nbsp;</div>
                </div>
            <?php endfor; ?>
        </div>
    <?php endfor; ?>
</div>

<div class="winner">
    &nbsp;
</div>