/*削除の仕方*/
/*drop table (table名)*/

CREATE TABLE IF NOT EXISTS `piano`.`iberia` (
  `number` INT NOT NULL,
  `title` VARCHAR(255) NOT NULL,
  `time` INT NOT NULL,
  `music_key` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`number`),
  UNIQUE INDEX `number_UNIQUE` (`number` ASC),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC))
ENGINE = InnoDB;

INSERT INTO iberia (number, title, time, music_key)
 VALUES
 (1,"Evocacion",6,"A flat minor"),
 (2,"El Puerto",4,"D flat Major"),
 (3,"Corpus-Christi en Sevilla",9,"F sharp minor"),
 (4,"Rondena",7,"D Major");