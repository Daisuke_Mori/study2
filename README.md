コンパイルの仕方
package.jsonの中のbuildを使う
コマンドはnpm run build
(6行目のbuildが実行される。npm-run-allは下に続くコマンドを全て実行。--parallelがないと、前のコマンドがないと先へ進めない。--watchがあると先に進まないので、並列して処理する必要がある。)