import {Box} from "./index";
export class GameRule {
    private boxes: Box[];
    private targetMaru: boolean;

    constructor(boxes: Box[], targetMaru: boolean) {
        this.boxes = boxes;
        this.targetMaru = targetMaru;
    }

    private getBox(x: number, y: number) {
        return this.boxes.filter((box) => {
            return (box.x === x) && (box.y === y);
        })[0];
    }

    public isFinish() {
        //縦、横、斜めのいずれかが揃っていればtrueを返す。それぞれは以下の関数で別々に定義
        return this.isAlignVertical() || this.isAlignHorizontal() || this.isAlignTilt();
    }

    private isAlignVertical() {
        for (let x = 1; x <= 3; x++) {
            const box1 = this.getBox(x, 1);
            const box2 = this.getBox(x, 2);
            const box3 = this.getBox(x, 3);
            if (box1.isMaru() && box2.isMaru() && box3.isMaru()) {
                return true;
            }
            else
                if (box1.isBatsu() && box2.isBatsu() && box3.isBatsu())
            {
                return true;
            }
        }
        return false;
    }


    private isAlignHorizontal() {
        for (let y = 1; y <= 3; y++) {
            const box1 = this.getBox(1, y);
            const box2 = this.getBox(2, y);
            const box3 = this.getBox(3, y);
            if (box1.isMaru() && box2.isMaru() && box3.isMaru()) {
                return true;
            }
            else
                if (box1.isBatsu() && box2.isBatsu() && box3.isBatsu())
            {
                return true;
            }
        }
        return false;
    }

    private isAlignTilt() {
        let box1 = this.getBox(1, 1);
        let box2 = this.getBox(2, 2);
        let box3 = this.getBox(3, 3);
        if (box1.isMaru() && box2.isMaru() && box3.isMaru()) {
                return true;
            }
        else
            if (box1.isBatsu() && box2.isBatsu() && box3.isBatsu()) {
                return true;
            }

        box1 = this.getBox(1, 3);
        box2 = this.getBox(2, 2);
        box3 = this.getBox(3, 1);
        if (box1.isMaru() && box2.isMaru() && box3.isMaru()) {
            return true;
        }
        else
            if (box1.isBatsu() && box2.isBatsu() && box3.isBatsu())
        {
            return true;
        }
        return false;
    }
}