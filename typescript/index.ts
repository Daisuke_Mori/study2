//同じディレクトリにあるGamaRule.tsを読み込む
import {GameRule} from "./GameRule";
const TEBAN_PLAYER_1 = 0;
const TEBAN_PLAYER_2 = 2;
class Game {
    private teban: number = TEBAN_PLAYER_1;
    private boxes: Box[];
    private turns: number = 0;
    private $winner: JQuery = $(".winner");

    private isTeban1(): boolean {
        return this.teban === TEBAN_PLAYER_1;
    }

    private isTeban2(): boolean {
        return this.teban === TEBAN_PLAYER_2;
    }

    private checkFinish() {
        //GameRuleクラスのインスタンスを生成
        const gameRule = new GameRule(this.boxes, this.isTeban1());
        if (gameRule.isFinish()) {
            if (this.isTeban1()) {
                this.$winner.text("player1の勝利です。");
                this.gameEnd();
            } else {
                this.$winner.text("player2の勝利です。");
                this.gameEnd();
            }
        }
        else {
            if (this.turns === 9){
                this.$winner.text("引き分けです。");
            }
        }
    }


    private gameEnd() {
        this.boxes.map((box) => {
            //unbind()はイベントハンドラを削除
            box.$box.unbind();
        })
    }

    //初期化メソッド
    private initBoxes() {
        //型指定
        this.boxes = [];
        for (let y = 1; y <= 3; y++) {
            for (let x = 1; x <= 3; x++) {
                //配列にBoxクラスの要素を追加(下の部分にあるclass Box以降を参照)
                this.boxes.push(new Box(x, y));
            }
        }

        //"map((配列名) => "{})は、配列内のすべての要素について{}内を実行
        this.boxes.map((box) => {
            //jQueryオブジェクトがクリックされて時に{}内を実行
            box.$box.click(() => {
                //すでに◯も×も入っていない時のみに以下を実行する
                if (box.marubatsu === MARUBATSU_NONE) {
                    this.turns += 1;
                    box.setMaruBatsu(this.isTeban1());
                    this.checkFinish();
                    this.toggleTeban();
                }
            });
        });
    }

    private
    toggleTeban() {
        //isTeban1がTrue(this.teban=TEBAN_PLAYER1なら)Trueを返す
        //要するに、Player1の手番であればTrueを返す
        if (this.isTeban1()) {
            //Player1の手番だったのでPlayer2の手番に変える
            this.teban = TEBAN_PLAYER_2;
        } else {
            this.teban = TEBAN_PLAYER_1;
        }
    }

//ここがまず実行される
    public
    newGame() {
        //初期化メソッド
        this.initBoxes();
    }
}

const MARUBATSU_NONE = 0;
const MARUBATSU_MARU = 1;
const MARUBATSU_BATSU = 2;

export class Box {
    //ここから3行は型指定
    public $box: JQuery;
    public x: number;
    public y: number;
    //型指定、初期値を設定しておく
    //MARUBATSU_NONEという変数名を使うのは、スクリプトの中で数字が出てくると、それが何を意味するかわかりにくいため。
    public marubatsu: number = MARUBATSU_NONE;


    constructor(x: number, y: number) {
        //Boxクラスのインスタンスにxとyというプロパティを持たせる。これは引数で渡す。
        this.x = x;
        this.y = y;
        //下の文は、たとえば$(.box[data-x=1][data-y=3])などのような文になり、jQueryオブジェクトをプロパティとして持たせる。
        this.$box = $(".box[data-x=" + x + "][data-y=" + y + "]");
    }

    //これより下はBoxクラスのインスタンスが持つ関数
    public isMaru() {
        //以下の構文は、return以降が正しければtrueを返す
        return this.marubatsu === MARUBATSU_MARU;
    }

    public isBatsu() {
        return this.marubatsu === MARUBATSU_BATSU;

    }

    public setMaruBatsu(isTeban1: boolean) {
        if (this.marubatsu === MARUBATSU_NONE) {
            if (isTeban1) {
                this.marubatsu = MARUBATSU_MARU;
                this.$box.find(".maru_batsu").text("◯");
            } else {
                this.marubatsu = MARUBATSU_BATSU;
                this.$box.find(".maru_batsu").text("×");
            }
        }
    }
}

//Gameオブジェクトの生成
const game = new Game();
game.newGame();