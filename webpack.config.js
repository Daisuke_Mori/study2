const path = require('path');
const webpack = require("webpack");

module.exports = [
    {
        entry: {
            index: "./typescript/index.ts"
        },
        output: {
            path: path.resolve(__dirname, "./src/js/"),
            filename: "[name].min.js",
            publicPath: "/js/"
        },
        devtool: "source-map",
        module: {
            loaders: [
                {
                    test: /\.tsx?$/,
                    exclude: /(node_modules)/,
                    loaders: ["babel-loader", "ts-loader"]
                }
            ]
        },
        resolve: {
            extensions: [".webpack.js", ".web.js", ".ts", ".js"]
        },
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        ]
    }
];
